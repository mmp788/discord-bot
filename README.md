# Discord Bot #


## How do I get set up? ##

### Configuration ###
### Dependencies ###

- git ([https://git-scm.com/](https://git-scm.com/))
- nodejs ([https://nodejs.org/en/](https://nodejs.org/en/))

## How to run tests ##

## Deployment instructions ##

1. Download the source
2. Open NodeJS command prompt and `cd` to the path where the source is located
3. Update the config.json file with your bot login token and Youtube API key (see [https://developers.google.com/youtube/v3/getting-started](https://developers.google.com/youtube/v3/getting-started), you only want a basic key NOT OAuth)
3. Run `npm install`
4. Run `node index.js` or `npm test`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact